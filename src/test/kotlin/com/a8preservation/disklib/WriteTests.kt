/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.disklib

import junit.framework.TestCase.assertTrue
import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.io.File
import java.io.FileInputStream

class WriteTests {
    @Test
    fun `test ATX write correctness`() {
        var count = 0
        File("./src/test/resources").walk().forEach {
            if ("atx".equals(it.extension, true) && !it.name.contains("[b]")) {
                val tmpFile = File.createTempFile("test-atx-", ".atx")
                tmpFile.deleteOnExit()
                writeATX(tmpFile, readATX(it.inputStream()))
                val bb1 = it.readBytes()
                val bb2 = tmpFile.readBytes()
                assertEquals(bb1.size, bb2.size)
                assertTrue(bb1.copyOfRange(0,7) contentEquals bb2.copyOfRange(0,7))
                // ignore creator code / version
                assertTrue(bb1.copyOfRange(12,bb1.size-12) contentEquals bb2.copyOfRange(12,bb1.size-12))
                count++
            }
        }
        assertTrue(count > 0)
    }

    @Test
    fun `test ATX weak write correctness`() {
        val disk = readATR(FileInputStream(File("./src/test/resources/Master Diskette II (1980)(Atari)(US)[!].atr")))
        disk.tracks[0].sectors[0].weakOffset = 5
        val f = createTempFile()
        f.deleteOnExit()
        writeATX(f, disk)
        val disk2 = readATX(FileInputStream(f))
        val s = disk2.tracks[0].sectors[0]
        assertTrue(s.hasWeakData)
        assertEquals(5, s.weakOffset)
    }

    @Test
    fun `test ATX CRC error write correctness`() {
        val disk = readATR(FileInputStream(File("./src/test/resources/Master Diskette II (1980)(Atari)(US)[!].atr")))
        disk.tracks[0].sectors[0].crcError = true
        val f = createTempFile()
        f.deleteOnExit()
        writeATX(f, disk)
        val disk2 = readATX(FileInputStream(f))
        val s = disk2.tracks[0].sectors[0]
        assertTrue(s.crcError)
    }

    @Test
    fun `test ATR write correctness`() {
        var count = 0
        File("./src/test/resources").walk().forEach {
            if ("atr".equals(it.extension, true)) {
                val tmpFile = File.createTempFile("test-atr-", ".atr")
                tmpFile.deleteOnExit()
                writeATR(tmpFile, readATR(it.inputStream()))
                assertTrue(it.readBytes() contentEquals tmpFile.readBytes())
                count++
            }
        }
        assertTrue(count > 0)
    }

    @Test
    fun `test unprotected PRO to ATR write correctness`() {
        val tmpFile = File.createTempFile("test-atr-", ".atr")
        tmpFile.deleteOnExit()
        writeATR(tmpFile, readPRO(FileInputStream(File("./src/test/resources/Everest Explorer.pro"))))
        assertTrue(FileInputStream(File("./src/test/resources/Everest Explorer.atr")).readBytes() contentEquals tmpFile.readBytes())
    }
}
