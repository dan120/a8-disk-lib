/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.disklib

import junit.framework.TestCase.assertEquals
import org.junit.Test

class IOTests {
    @Test
    fun testByteArrayRead4ByteLittleEndian() {
        assertEquals(192, byteArrayOf(0xC0.toByte(), 0x00.toByte(), 0x00.toByte(), 0x00.toByte()).read4ByteLittleEndian(0))
        assertEquals(398344, byteArrayOf(0x08.toByte(), 0x14.toByte(), 0x06.toByte(), 0x00.toByte()).read4ByteLittleEndian(0))
    }

    @Test
    fun testConvertFluxStreamToBytes() {
        assertEquals("0000", convertFluxStreamToBitString(doubleArrayOf(8.0,8.0,8.0,8.0)))

        assertEquals("11", convertFluxStreamToBitString(doubleArrayOf(4.0,4.0,4.0,4.0)))
        assertEquals("11", convertFluxStreamToBitString(doubleArrayOf(3.08,3.08,3.08,3.08)))
        assertEquals("11", convertFluxStreamToBitString(doubleArrayOf(4.92,4.92,4.92,4.92)))
        assertEquals("", convertFluxStreamToBitString(doubleArrayOf(3.07,3.07,3.07,3.07)))
        assertEquals("", convertFluxStreamToBitString(doubleArrayOf(4.93,4.93,4.93,4.93)))

        assertEquals("0", convertFluxStreamToBitString(doubleArrayOf(4.0,8.0)))

        assertEquals("000101011", convertFluxStreamToBitString(doubleArrayOf(8.0,8.0,8.0,4.0,4.0,8.0,4.0,4.0,8.0,4.0,4.0,4.0,4.0)))
        assertEquals("10001111", convertFluxStreamToBitString(doubleArrayOf(4.0,4.0,8.0,8.0,8.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0)))
    }
}