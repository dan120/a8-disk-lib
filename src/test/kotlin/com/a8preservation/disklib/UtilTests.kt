package com.a8preservation.disklib

import junit.framework.TestCase.assertEquals
import org.junit.Test

class UtilTests {
    @Test
    fun testConvertSectorLetterToNumber() {
        assertEquals(1, convertSectorLetterToNumber('1'))
        assertEquals(2, convertSectorLetterToNumber('2'))
        assertEquals(3, convertSectorLetterToNumber('3'))
        assertEquals(4, convertSectorLetterToNumber('4'))
        assertEquals(5, convertSectorLetterToNumber('5'))
        assertEquals(6, convertSectorLetterToNumber('6'))
        assertEquals(7, convertSectorLetterToNumber('7'))
        assertEquals(8, convertSectorLetterToNumber('8'))
        assertEquals(9, convertSectorLetterToNumber('9'))
        assertEquals(10, convertSectorLetterToNumber('A'))
        assertEquals(11, convertSectorLetterToNumber('B'))
        assertEquals(12, convertSectorLetterToNumber('C'))
        assertEquals(13, convertSectorLetterToNumber('D'))
        assertEquals(14, convertSectorLetterToNumber('E'))
        assertEquals(15, convertSectorLetterToNumber('F'))
        assertEquals(16, convertSectorLetterToNumber('G'))
        assertEquals(17, convertSectorLetterToNumber('H'))
        assertEquals(18, convertSectorLetterToNumber('I'))
    }

    @Test
    fun testConvertSectorNumberToLetter() {
        assertEquals('1', convertSectorNumberToLetter(1))
        assertEquals('2', convertSectorNumberToLetter(2))
        assertEquals('3', convertSectorNumberToLetter(3))
        assertEquals('4', convertSectorNumberToLetter(4))
        assertEquals('5', convertSectorNumberToLetter(5))
        assertEquals('6', convertSectorNumberToLetter(6))
        assertEquals('7', convertSectorNumberToLetter(7))
        assertEquals('8', convertSectorNumberToLetter(8))
        assertEquals('9', convertSectorNumberToLetter(9))
        assertEquals('A', convertSectorNumberToLetter(10))
        assertEquals('B', convertSectorNumberToLetter(11))
        assertEquals('C', convertSectorNumberToLetter(12))
        assertEquals('D', convertSectorNumberToLetter(13))
        assertEquals('E', convertSectorNumberToLetter(14))
        assertEquals('F', convertSectorNumberToLetter(15))
        assertEquals('G', convertSectorNumberToLetter(16))
        assertEquals('H', convertSectorNumberToLetter(17))
        assertEquals('I', convertSectorNumberToLetter(18))
    }

    @Test
    fun testCreateInterleaveMap() {
        var map = createInterleaveMappingArray("123456789ABCDEFGHI")
        for (i in 1..18) {
            assertEquals(i, map[i])
        }
        map = createInterleaveMappingArray("IHGFEDCBA987654321")
        for (i in 18 downTo 1) {
            assertEquals(i, map[19 - i])
        }
    }

    @Test
    fun testGetTrackNumberForAbsoluteSectorNumber() {
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(1, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(2, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(3, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(4, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(5, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(6, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(7, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(8, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(9, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(10, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(11, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(12, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(13, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(14, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(15, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(16, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(17, 18))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(18, 18))
        assertEquals(1, getTrackNumberForAbsoluteSectorNumber(19, 18))
        assertEquals(39, getTrackNumberForAbsoluteSectorNumber(720, 18))
        assertEquals(35, getTrackNumberForAbsoluteSectorNumber(639, 18))

        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(1, 26))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(2, 26))
        assertEquals(0, getTrackNumberForAbsoluteSectorNumber(26, 26))
        assertEquals(1, getTrackNumberForAbsoluteSectorNumber(27, 26))
        assertEquals(39, getTrackNumberForAbsoluteSectorNumber(1040, 26))
    }

    @Test
    fun testGetTrackSectorIndexForAbsoluteSectorNumber() {
        assertEquals(1, getTrackSectorNumberForAbsoluteSectorNumber(1, 18))
        assertEquals(2, getTrackSectorNumberForAbsoluteSectorNumber(2, 18))
        assertEquals(18, getTrackSectorNumberForAbsoluteSectorNumber(720, 18))

        assertEquals(1, getTrackSectorNumberForAbsoluteSectorNumber(1, 26))
        assertEquals(2, getTrackSectorNumberForAbsoluteSectorNumber(2, 26))
        assertEquals(26, getTrackSectorNumberForAbsoluteSectorNumber(1040, 26))
    }

    @Test
    fun testGetAbsoluteSectorForTrackAndSectorIndex() {
        assertEquals(1, getAbsoluteSectorForTrackAndSectorIndex(0, 1, 18))
        assertEquals(2, getAbsoluteSectorForTrackAndSectorIndex(0, 2, 18))
        assertEquals(720, getAbsoluteSectorForTrackAndSectorIndex(39, 18, 18))
        assertEquals(1040, getAbsoluteSectorForTrackAndSectorIndex(39, 26, 26))
    }

    @Test
    fun testGetAbsoluteSectorForTrackAndSectorString() {
        assertEquals(1, getAbsoluteSectorForTrackAndSectorString("t00s01", 18))
        assertEquals(19, getAbsoluteSectorForTrackAndSectorString("t01s01", 18))
        assertEquals(720, getAbsoluteSectorForTrackAndSectorString("t39s18", 18))
    }
}