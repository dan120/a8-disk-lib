/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.disklib

import junit.framework.TestCase.*
import org.junit.Test
import java.io.File
import java.io.FileInputStream

class ModelTests {
    @Test
    fun testGetSectorFromDisk() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Agent U.S.A. (1984)(Scholastic)(US)[!].atx")))
        assertEquals(1, disk.getSector(1).size)
        assertEquals(1, disk.getSector(1)[0].number)
        assertEquals(1, disk.getSector(10).size)
        assertEquals(2, disk.getSector(19).size)
        assertEquals(0, disk.getSector(1000).size)
    }

    @Test
    fun testReadDiskWithWeakSector() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Goonies, The (1985)(Datasoft)(US)[!].atx")))
        assertEquals(1, disk.getSector(720).size)
        assertEquals(18, disk.getSector(720)[0].number)
        assertTrue(disk.getSector(720)[0].hasWeakData)
    }

    @Test
    fun testDeleteLastTracks() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Action Quest (1982)(JV Software)(US)[b].atx")))
        assertEquals(40, disk.trackCount)
        disk.deleteLastTracks(1)
        assertEquals(39, disk.trackCount)
        disk.deleteLastTracks(1)
        assertEquals(38, disk.trackCount)
    }

    @Test
    fun testInsertEmptySector() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Action Quest (1982)(JV Software)(US)[b].atx")))
        val track = disk.tracks[0]
        assertEquals(18, track.sectors.size)
        track.insertEmptySector(1,5000)
        assertEquals(19, track.sectors.size)
        assertEquals(1, track.sectors[4].number)
        assertEquals(5000, track.sectors[4].position)
    }

    @Test
    fun testGetAbsoluteSectorIndexByPosition() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Action Quest (1982)(JV Software)(US)[b].atx")))
        val track = disk.tracks[0]
        assertNull(track.getSectorByPosition(6637))
        assertEquals(5, track.getSectorByPosition(6638)!!.index)
    }

    @Test
    fun testReplaceSectorData() {
        val disk = readATX(FileInputStream(File("./src/test/resources/Action Quest (1982)(JV Software)(US)[b].atx")))
        val dstTrack = disk.tracks[9]
        val dstSectorIx = dstTrack.getSectorByPosition(5602)
        assertNotNull(dstSectorIx)
        assertEquals(3265854109L, dstSectorIx!!.value.dataCrc.value)
        assertEquals(3684791464L, dstSectorIx.value.dataCrcMinus3.value)
        assertEquals(8, dstSectorIx.value.fdcStatus)

        val srcSectorIx = disk.tracks[4].getSectorByPosition(10586)
        assertNotNull(srcSectorIx)
        assertEquals(3197097400L, srcSectorIx!!.value.dataCrc.value)
        assertEquals(81904948L, srcSectorIx.value.dataCrcMinus3.value)
        assertEquals(0, srcSectorIx.value.fdcStatus)

        dstTrack.replaceSectorData(dstSectorIx.index, srcSectorIx.value)
        val dstSectorIx2 = dstTrack.getSectorByPosition(5602)
        assertEquals(3197097400L, dstSectorIx2!!.value.dataCrc.value)
        assertEquals(81904948L, dstSectorIx2.value.dataCrcMinus3.value)
        assertEquals(0, dstSectorIx2.value.fdcStatus)
    }

    @Test
    fun testSectorSetWeakOffset() {
        // test setting null weak status to null again
        var sector = Sector(1, byteArrayOf(1), 0, 0)
        assertEquals(0, sector.fdcStatus)
        sector.weakOffset = null
        assertEquals(0, sector.fdcStatus)

        // test setting existing weak status to null
        sector = Sector(1, byteArrayOf(1,2,3,4), 0, 0)
        sector.weakOffset = 1
        assertEquals(64, sector.fdcStatus)
        sector.weakOffset = null
        assertEquals(0, sector.fdcStatus)

        // test setting existing weak status and CRC error to null
        sector = Sector(1, byteArrayOf(1,2,3,4), 0, 8)
        sector.weakOffset = 1
        assertEquals(72, sector.fdcStatus)
        sector.weakOffset = null
        assertEquals(8, sector.fdcStatus)
    }
}