package com.a8preservation.disklib

import junit.framework.TestCase.*
import org.junit.Test
import java.io.FileInputStream

internal class ProTests {
    @Test
    fun testCalculateSectorPosition() {
        assertEquals(0, calculateSectorPosition(1, 18, "123456789ABCDEFGHI"))
        assertEquals(24582, calculateSectorPosition(18, 18, "123456789ABCDEFGHI"))
        assertEquals(1446, calculateSectorPosition(1, 18, "213456789ABCDEFGHI"))
        assertEquals(0, calculateSectorPosition(1, 26, "123456789ABCDEFGHIJKLMNOPQ"))
        assertEquals(1001, calculateSectorPosition(2, 26, "123456789ABCDEFGHIJKLMNOPQ"))
        assertEquals(25025, calculateSectorPosition(26, 26, "123456789ABCDEFGHIJKLMNOPQ"))

        try {
            assertEquals(0, calculateSectorPosition(1, 26, "123456789ABCDEFGHI"))
            fail("Should have thrown exception")
        } catch (e: IllegalArgumentException) {}
    }

    @Test
    fun testFindFirstMissingSectorNumber() {
        assertEquals(1, findFirstMissingSectorNumber(mutableListOf(), 18))
        assertEquals(1, findFirstMissingSectorNumber(mutableListOf(Sector(2, null, 0)), 18))
        assertEquals(2, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(3, null, 2892)), 18))
        assertEquals(3, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(2, null, 1446)), 18))
        assertEquals(17, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(2, null, 1446), Sector(3, null, 2892), Sector(4, null, 4338), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(18, null, 23136)), 18))
        assertEquals(18, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(2, null, 1446), Sector(3, null, 2892), Sector(4, null, 4338), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(17, null, 21690)), 18))
        assertEquals(-1, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(2, null, 1446), Sector(3, null, 2892), Sector(4, null, 4338), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(17, null, 21690), Sector(18, null, 23136)), 18))
        assertEquals(25, findFirstMissingSectorNumber(mutableListOf(Sector(1, null, 0), Sector(2, null, 1446), Sector(3, null, 2892), Sector(4, null, 4338), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(17, null, 21690), Sector(18, null, 23136), Sector(19, null, 23136), Sector(20, null, 23136), Sector(21, null, 23136), Sector(22, null, 23136), Sector(23, null, 23136), Sector(24, null, 23136), Sector(26, null, 23136)), 26))
    }

    @Test
    fun testInsertPhantomSectorWithSimpleDuplicate() {
        val sectorList = mutableListOf<Sector>()
        sectorList.add(Sector(1, null))
        sectorList.add(Sector(3, null))
        sectorList.add(Sector(4, null))
        sectorList.add(Sector(5, null))
        sectorList.add(Sector(6, null))
        sectorList.add(Sector(7, null))
        sectorList.add(Sector(8, null))
        sectorList.add(Sector(9, null))
        sectorList.add(Sector(10, null))
        sectorList.add(Sector(11, null))
        sectorList.add(Sector(12, null))
        sectorList.add(Sector(13, null))
        sectorList.add(Sector(14, null))
        sectorList.add(Sector(15, null))
        sectorList.add(Sector(16, null))
        sectorList.add(Sector(17, null))
        sectorList.add(Sector(18, null))
        val phantom = PhantomSector(ProSectorHeader(), null)

        assertEquals(17, sectorList.size)
        insertPhantomSectorIntoSectorList(1, phantom, sectorList, 18, DEFAULT_SD_INTERLEAVE)
        assertEquals(18, sectorList.size)
        assertEquals(1, sectorList[17].number)
        assertEquals(13014, sectorList[17].position)
    }

    @Test
    fun testInsertSDPhantomSectorWithNoSpaceForDuplicate() {
        val sectorList = mutableListOf(Sector(1, null, 0), Sector(2, null, 1446), Sector(3, null, 2892), Sector(4, null, 4338), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(17, null, 21690), Sector(18, null, 23136))

        assertEquals(18, sectorList.size)
        insertPhantomSectorIntoSectorList(1, PhantomSector(ProSectorHeader(), null), sectorList, 18, DEFAULT_SD_INTERLEAVE)
        assertEquals(19, sectorList.size)
        assertEquals(1, sectorList[18].number)
        assertEquals(13021, sectorList[18].position)

        insertPhantomSectorIntoSectorList(18, PhantomSector(ProSectorHeader(), null), sectorList, 18, DEFAULT_SD_INTERLEAVE)
        assertEquals(20, sectorList.size)
        assertEquals(18, sectorList[19].number)
        assertEquals(10115, sectorList[19].position)
    }

    @Test
    fun testInsertSDPhantomSectorWithNoSpaceForDuplicate2() {
        val sectorList = mutableListOf(Sector(37, null, 0), Sector(38, null, 13014), Sector(39, null, 1446), Sector(40, null, 14460), Sector(5, null, 5784), Sector(6, null, 7230), Sector(7, null, 0), Sector(8, null, 8676), Sector(9, null, 10122), Sector(10, null, 11568), Sector(11, null, 13014), Sector(12, null, 14460), Sector(13, null, 15096), Sector(14, null, 17352), Sector(15, null, 18798), Sector(16, null, 20244), Sector(17, null, 21690), Sector(18, null, 23136))

        assertEquals(18, sectorList.size)
        insertPhantomSectorIntoSectorList(1, PhantomSector(ProSectorHeader(), null), sectorList, 18, DEFAULT_SD_INTERLEAVE)
        assertEquals(19, sectorList.size)
        assertEquals(1, sectorList[18].number)
        assertEquals(13021, sectorList[18].position)
    }

    @Test
    fun testInsertEDPhantomSectorWithNoSpaceForDuplicate() {
        val sectorList = mutableListOf(Sector(1, null, 0), Sector(2, null, 1001), Sector(3, null, 2002), Sector(4, null, 3003), Sector(5, null, 4004), Sector(6, null, 5005), Sector(7, null, 6006), Sector(8, null, 7007), Sector(9, null, 8008), Sector(10, null, 9009), Sector(11, null, 10010), Sector(12, null, 11011), Sector(13, null, 12012), Sector(14, null, 13013), Sector(15, null, 14014), Sector(16, null, 15015), Sector(17, null, 16016), Sector(18, null, 17017), Sector(19, null, 18018), Sector(20, null, 19019), Sector(21, null, 20020), Sector(22, null, 21021), Sector(23, null, 22022), Sector(24, null, 23023), Sector(25, null, 24024), Sector(26, null, 25025))

        assertEquals(26, sectorList.size)
        insertPhantomSectorIntoSectorList(1, PhantomSector(ProSectorHeader(), null), sectorList, 26, DEFAULT_ED_INTERLEAVE)
        assertEquals(27, sectorList.size)
        assertEquals(1, sectorList[26].number)
        assertEquals(13021, sectorList[26].position)

        insertPhantomSectorIntoSectorList(26, PhantomSector(ProSectorHeader(), null), sectorList, 26, DEFAULT_SD_INTERLEAVE)
        assertEquals(28, sectorList.size)
        assertEquals(26, sectorList[27].number)
        assertEquals(12004, sectorList[27].position)
    }

    @Test
    fun teadReadPROWithCRCError() {
        val disk = readPRO(FileInputStream("./src/test/resources/Adventures of Buckaroo Banzai, The (19xx)(Adventure International)(US)(Side B).pro"), "123456789ABCDEFGHI")

        // assert correct number of tracks
        assertEquals(40, disk.trackCount)

        // assert each track has 18 sectors & sector positions are correct based on interleave
        for (trackNum in 0..39) {
            assertEquals(18, disk.tracks[trackNum].sectorCount)
            var lastPos = -1
            for (s in disk.tracks[trackNum].sectors) {
                assertTrue(s.position > lastPos)
                lastPos = s.position
            }
        }

        assertEquals(1, disk.tracks[0].sectors[0].number)
        assertEquals(0, disk.tracks[0].sectors[0].position)
        assertEquals(2, disk.tracks[0].sectors[1].number)
        assertEquals(1446, disk.tracks[0].sectors[1].position)
        assertEquals(18, disk.tracks[0].sectors[17].number)
        assertEquals(24582, disk.tracks[0].sectors[17].position)

        // check normal sector data
        assertEquals(2290690188L, disk.getSector(1)[0].dataCrc.value)
        assertEquals(1830344605L, disk.getSector(360)[0].dataCrc.value)
        assertEquals(3265854109L, disk.getSector(720)[0].dataCrc.value)

        // check that only sector 5 has a CRC error
        for (i in 1..719) {
            val s = disk.getSector(i)[0]
            if (i == 5) {
                assertTrue(s.crcError)
            } else {
                assertFalse(s.crcError)
            }
        }
    }

    @Test
    fun `test read PRO with descending interleave`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Adventures of Buckaroo Banzai, The (19xx)(Adventure International)(US)(Side B).pro"), "IHGFEDCBA987654321")

        // assert correct number of tracks
        assertEquals(40, disk.trackCount)

        // assert each track has 18 sectors & sector positions are correct based on interleave
        for (trackNum in 0..39) {
            assertEquals(18, disk.tracks[trackNum].sectorCount)
            for (sectorIndex in 0..17) {
                assertEquals(18 - sectorIndex, disk.tracks[sectorIndex].sectors[sectorIndex].number)
            }
            var lastPos = -1
            for (s in disk.tracks[trackNum].sectors) {
                assertTrue(s.position > lastPos)
                lastPos = s.position
            }
        }

        assertEquals(18, disk.tracks[0].sectors[0].number)
        assertEquals(0, disk.tracks[0].sectors[0].position)
        assertEquals(17, disk.tracks[0].sectors[1].number)
        assertEquals(1446, disk.tracks[0].sectors[1].position)
        assertEquals(1, disk.tracks[0].sectors[17].number)
        assertEquals(24582, disk.tracks[0].sectors[17].position)
    }

    @Test
    fun `test read PRO with duplicates`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Jumpman.pro"))
        assertEquals(2, disk.getSector(703).size)
    }

    @Test
    fun `test read PRO with 19 sectors in track`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Archon I - The Light and The Dark.pro"))
        assertEquals(TrackEncoding.FM, disk.tracks[0].encoding)
        assertEquals(2, disk.getSector(41).size)
        assertEquals(2892, disk.getSector(41)[0].position)
        assertEquals(15913, disk.getSector(41)[1].position)
        assertEquals(1, disk.tracks[0].sectors[0].number)
        assertEquals("c7689825", String.format("%08x", disk.tracks[0].sectors[0].dataCrc.value))
    }

    @Test
    fun `test read PRO and set interleave`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Adventures of Buckaroo Banzai, The (19xx)(Adventure International)(US)(Side B).pro"))
        disk.tracks[0].setInterleave("I7E3AH6D29G5C18F4B")
        assertEquals(18, disk.tracks[0].sectors[0].number)
        assertEquals(0, disk.tracks[0].sectors[0].position)
        assertEquals(7, disk.tracks[0].sectors[1].number)
        assertEquals(1446, disk.tracks[0].sectors[1].position)
        assertEquals(14, disk.tracks[0].sectors[2].number)
        assertEquals(2892, disk.tracks[0].sectors[2].position)
    }

    @Test
    fun `test read PRO and set interleave and missing sectors`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Buchstabenlotterie - D7 - DXG 5705.PRO"))
        disk.tracks[0].setInterleave("13579BDFH2468ACEGI")
        assertEquals(1, disk.tracks[0].sectors[0].number)
        assertEquals(0, disk.tracks[0].sectors[0].position)
        assertEquals(3, disk.tracks[0].sectors[1].number)
        assertEquals(1446, disk.tracks[0].sectors[1].position)
        assertEquals(5, disk.tracks[0].sectors[2].number)
        assertEquals(2892, disk.tracks[0].sectors[2].position)
        assertEquals(7, disk.tracks[0].sectors[3].number)
        assertEquals(4338, disk.tracks[0].sectors[3].position)
        assertEquals(17, disk.tracks[0].sectors[4].number)
        assertEquals(11568, disk.tracks[0].sectors[4].position)
        assertEquals(2, disk.tracks[0].sectors[5].number)
        assertEquals(13014, disk.tracks[0].sectors[5].position)
    }

    @Test
    fun `test read ED PRO with originalSector count`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Major Bronx (L.K. Avalon - 1992) (AD02392).pro"))
        assertEquals(DiskDensity.MEDIUM, disk.density)
        assertEquals(40, disk.trackCount)
        assertEquals(128, disk.sectorSize)
        assertEquals(26, disk.sectorsPerTrack)
        assertEquals(26, disk.tracks[0].sectors.size)
        assertEquals(TrackEncoding.MFM, disk.tracks[0].encoding)
        assertEquals(1, disk.tracks[0].sectors[0].number)
        assertEquals("b13bbf17", String.format("%08x", disk.tracks[0].sectors[0].dataCrc.value))
        assertEquals(25, disk.tracks[0].sectors[12].number)
        assertEquals("cc0bc7e6", String.format("%08x", disk.tracks[0].sectors[12].dataCrc.value))
        assertEquals(12, disk.tracks[0].sectors[18].number)
        assertEquals("20855a1a", String.format("%08x", disk.tracks[0].sectors[18].dataCrc.value))
    }

    @Test
    fun `test read ED PRO without originalSector count`() {
        val disk = readPRO(FileInputStream("./src/test/resources/Print Star II (1989)(AMC Verlag)(DE)(Side A)[req 64K][h replicated protection].pro"))
        assertEquals(DiskDensity.MEDIUM, disk.density)
        assertEquals(40, disk.trackCount)
        assertEquals(128, disk.sectorSize)
        assertEquals(26, disk.sectorsPerTrack)
        assertEquals(26, disk.tracks[0].sectors.size)
    }
}