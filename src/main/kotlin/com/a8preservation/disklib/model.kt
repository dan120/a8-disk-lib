/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.disklib

import java.io.ByteArrayOutputStream
import java.nio.charset.Charset
import java.util.zip.CRC32

enum class DiskDensity {
    SINGLE, MEDIUM, DOUBLE
}

enum class TrackEncoding {
    FM, MFM
}

/**
 * A class representing an Atari 8-bit floppy disk.
 */
class Disk(val density: DiskDensity, var tracks: List<Track>, val name: String? = null) {
    /**
     * The number of tracks on the disk
     */
    val trackCount get() = tracks.size

    /**
     * The number of bytes in each sector
     */
    val sectorSize get() = tracks.firstOrNull()?.sectorSize

    /**
     * The number of sectors per track
     */
    val sectorsPerTrack get() = if (density == DiskDensity.MEDIUM) 26 else 18

    /**
     * Returns the first track with the given number (0-39).
     */
    fun getTrack(num: Int): Track {
        return tracks[tracks.indexOfFirst { it.number == num }]
    }

    /**
     * Returns a sector by absolute number.
     */
    fun getSector(absoluteNum: Int): List<Sector> {
        return if (absoluteNum < 1 || absoluteNum > tracks.size * sectorsPerTrack) {
            listOf()
        } else {
            val track = tracks[getTrackNumberForAbsoluteSectorNumber(absoluteNum, sectorsPerTrack)]
            val sectorIdx = getTrackSectorNumberForAbsoluteSectorNumber(absoluteNum, sectorsPerTrack)
            track.sectors.filter { it.number == sectorIdx}
        }
    }

    /**
     * Returns the volume table of contents (VTOC) for the disk.
     */
    fun getVTOC(): VTOC? {
        val sl = getSector(360)
        if (sl.size == 1) {
            val sector = sl.first()
            if (sector.hasData) {
                return VTOC(sector.data!!.read2ByteLittleEndian(1), sector.data!!.read2ByteLittleEndian(3))
            }
        }
        return null
    }

    /**
     * Returns the file directory for the disk.
     */
    fun getFileDirectory(): List<DOSFile> {
        val DIR_ENTRY_SIZE = 16
        val s = getSector(1).first()
        if (s.data?.get(0)?.toInt() == 0) {
            val results = ArrayList<DOSFile>()
            for (num in 361..368) {
                val sl = getSector(num)
                if (sl.size == 1) {
                    val sector = sl.first()
                    if (sector.hasData) {
                        for (i in 0..7) {
                            val str = sector.data!!.toString(Charset.defaultCharset())
                            val file = DOSFile(
                                sector.data!![i*DIR_ENTRY_SIZE],
                                sector.data!!.read2ByteLittleEndian(i * DIR_ENTRY_SIZE + 1),
                                sector.data!!.read2ByteLittleEndian(i * DIR_ENTRY_SIZE + 3),
                                str.substring(i * DIR_ENTRY_SIZE + 5, i * DIR_ENTRY_SIZE + 13),
                                str.substring(i * DIR_ENTRY_SIZE + 13, i * DIR_ENTRY_SIZE + 16)
                            )
                            if (file.isDOS2 && file.isUsed) {
                                results.add(file)
                            }
                        }
                    } else {
                        throw RuntimeException("Unable to read directory; sector $num has no data")
                    }
                } else {
                    throw RuntimeException("Unable to read directory; sector $num has duplicates")
                }
            }
            return results
        } else {
            throw RuntimeException("This command only supports Atari DOS disks")
        }
    }

    /**
     * Creates an byte buffer of all data by track and sector index.
     */
    fun createByteBuffer(): ByteArray {
        val b = ByteArrayOutputStream()
        for (track in tracks) {
            for (sector in track.sectors) {
                b.write(sector.data)
            }
        }
        return b.toByteArray()
    }

    /**
     * Deletes the last x number of tracks.
     */
    fun deleteLastTracks(num: Int) {
        tracks = tracks.dropLast(num)
    }

    /**
     * Renumber all tracks starting with 0.
     */
    fun renumberTracks() {
        val newList = ArrayList<Track>()
        for ((i,t) in tracks.withIndex()) {
            newList.add(Track(i, t.encoding, t.sectors))
        }
        tracks = newList
    }
}

/**
 * A class representing an Atari 8-bit floppy disk track.
 *
 * @param number the track number (0-39)
 * @param encoding the track encoding
 * @param sectors the list of sector comprising the track
 */
class Track(val number: Int, val encoding: TrackEncoding, var sectors: List<Sector>, val driveRpm: Int? = null, val flux: List<FluxData>? = null) {
    /**
     * The number of bytes in each sector
     */
    val sectorSize get() = sectors.firstOrNull()?.data?.size

    /**
     * The number of sectors in the track
     */
    val sectorCount get() = sectors.size

    /**
     * The number of flux data entries for this track
     */
    val fluxCount get() = flux?.size ?: 0

    /**
     * Returns the first sector with the given number.
     *
     * @param num the sector number (1-18)
     */
    fun getFirstSector(num: Int): Sector? {
        val ix = sectors.indexOfFirst { it.number == num }
        return if (ix > -1) sectors[ix] else null
    }

    fun getSectorByPosition(pos: Int): IndexedValue<Sector>? {
        for ((i,s) in sectors.withIndex()) {
            if (s.position == pos) {
                return IndexedValue(i, s)
            }
        }
        return null
    }

    fun setInterleave(interleave: String) {
        val map = createInterleaveMappingArray(interleave)
        sectors = sectors.sortedBy { map.indexOf(it.number) }
        for (s in sectors) {
            s.position = (map.indexOf(s.number) - 1) * 1446
        }
    }

    fun insertEmptySector(number: Int, position: Int) {
        val newSectors = mutableListOf<Sector>()
        newSectors.addAll(0, sectors.filter { it.position < position })
        newSectors.add(Sector(number, position = position))
        newSectors.addAll(newSectors.size, sectors.filter { it.position > position })
        sectors = newSectors
    }

    fun replaceSectorData(index: Int, sector: Sector) {
        sectors[index].weakOffset = sector.weakOffset
        sectors[index].data = sector.data
        sectors[index].fdcStatus = sector.fdcStatus
    }
}

/**
 * A class representing an Atari 8-bit floppy disk sector.
 *
 * @param number the sector number (1-18)
 * @param data the sector data
 * @param position the angular position of the sector on the track
 * @param fdcStatus the floppy controller status for the sector
 * @param description an optional description for this sector
 */
class Sector(val number: Int, data: ByteArray? = null, var position: Int = 0, var fdcStatus: Int = 0, var description: String? = null) {
    /**
     * The data offset at which weak data starts within the sector (or null if there is no weak data)
     */
    var weakOffset: Int? = null
        set(weakOffset) {
            fdcStatus = if (weakOffset != null) (fdcStatus or 0x40) else (fdcStatus and 0x3f)
            dataCrc = calculateDataCrc(data, weakOffset)
            field = weakOffset
        }

    /**
     * Indicates if the sector has any weak data.
     */
    val hasWeakData get() = weakOffset != null

    var crcError: Boolean
        get() = (((fdcStatus shr 3) and 1) > 0)
        set(b) { fdcStatus = if (b) fdcStatus or 0x08 else fdcStatus and 0xF7 }

    val isDeleted get() = (((fdcStatus shr 5) and 1) > 0)

    val isMissing get() = (((fdcStatus shr 4) and 1) > 0)

    val isLong get() = (((fdcStatus shr 2) and 1) > 0)

    val hasError get() = (isDeleted || isMissing || isLong || crcError || hasWeakData)

    val hasData get() = data != null

    val hasDescription get() = description != null

    /**
     * A CRC for all relevant data in the sector.
     */
    var dataCrc: CRC32 = CRC32()
    var dataCrcMinus3: CRC32 = CRC32()

    /**
     * The sector data.
     */
    var data: ByteArray? = null
        set(data) {
            if (data != null) {
                dataCrc = calculateDataCrc(data, weakOffset)
                dataCrcMinus3 = calculateDataCrc(data.slice(IntRange(0, data.size - 4)).toByteArray(), weakOffset)
            }
            field = data
        }

    init {
        this.data = data
    }

    /**
     * Indicates if the sector is empty (data bytes are all 0)
     */
    fun isEmpty(): Boolean {
        if (data != null) {
            for (b in data!!.iterator()) {
                if (b.compareTo(0) != 0) {
                    return false
                }
            }
        }
        return true
    }
}

data class VTOC(val availableSectorCount: Int, val unusedSectorCount: Int)

class DOSFile(val flagByte: Byte, val sectorCount: Int, val startSector: Int, val filename: String, val extension: String) {
    val isOpened get() = (flagByte.toInt() and 0x01) > 0
    val isDOS2 get() = (flagByte.toInt() and 0x02) > 0
    val isLocked get() = (flagByte.toInt() and 0x20) > 0
    val isUsed get() = (flagByte.toInt() and 0x40) > 0
    val isDeleted get() = (flagByte.toInt() and 0x80) > 0
}

data class FluxData(val rawData: DoubleArray, var byteData: ByteArray)

