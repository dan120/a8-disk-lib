/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.disklib

import java.io.BufferedInputStream
import java.io.OutputStream

fun BufferedInputStream.read2ByteBigEndian(): Int = this.read().toUByte().toInt() * 256 + this.read().toUByte().toInt()
fun BufferedInputStream.read2ByteLittleEndian(): Int = this.read().toUByte().toInt() + this.read().toUByte().toInt() * 256
fun BufferedInputStream.read4ByteLittleEndian(): Int = (this.read() and 0xff) or ((this.read() and 0xff) shl 8) or ((this.read() and 0xff) shl 16) or ((this.read() and 0xff) shl 24)

fun ByteArray.readByte(offset: Int): Int = this[offset].toInt() and 0xff
fun ByteArray.read2ByteLittleEndian(offset: Int): Int = this[offset].toUByte().toInt() + this[offset+1].toUByte().toInt() * 256
fun ByteArray.read2ByteBigEndian(offset: Int): Int = this[offset].toUByte().toInt() * 256 + this[offset+1].toUByte().toInt()
fun ByteArray.read4ByteLittleEndian(offset: Int): Int = (this[offset].toInt() and 0xff) or ((this[offset+1].toInt() and 0xff) shl 8) or ((this[offset+2].toInt() and 0xff) shl 16) or ((this[offset+3].toInt() and 0xff) shl 24)

private val HEX_CHARS = "0123456789ABCDEF".toCharArray()

fun ByteArray.toHexString(): String {
    val result = StringBuffer()
    forEach {
        val octet = it.toInt()
        val firstIndex = (octet and 0xF0).ushr(4)
        val secondIndex = octet and 0x0F
        result.append(HEX_CHARS[firstIndex])
        result.append(HEX_CHARS[secondIndex])
        result.append(" ")
    }
    return result.toString()
}

fun writeByte(out: OutputStream, num: Int) {
    out.write(num)
}

fun write2Bytes(out: OutputStream, num: Int) {
    out.write(num and 0xFF)
    out.write((num shr 8) and 0xFF)
}

fun write4Bytes(out: OutputStream, num: Int) {
    out.write(num and 0xFF)
    out.write((num shr 8) and 0xFF)
    out.write((num shr 16) and 0xFF)
    out.write((num shr 24) and 0xFF)
}

fun convertFluxStreamToBytes(flux: DoubleArray): ByteArray {
    // TODO
    return ByteArray(0)
}

fun normalizeTransition(v: Double): Int {
    // window comparator of +/- 24%
    return when (v) {
        in 3.08..4.92 -> 4
        in 6.16..9.84 -> 8
        else -> 0
    }
}

fun convertFluxStreamToBitString(flux: DoubleArray): String {
    val bits = StringBuffer()

    var totalTime = 0.0
    var clockBit = false
    var errors = 0
    for (cv in flux) {
        when (normalizeTransition(cv)) {
            4 -> {
                clockBit = if (clockBit) {
                    bits.append("1")
                    false
                } else {
                    true
                }
            }
            8 -> {
                bits.append("0")
                clockBit = true
            }
            else -> errors++
        }
        totalTime += cv
    }
    println("Errors: $errors; Total time $totalTime")
    return bits.toString()
}