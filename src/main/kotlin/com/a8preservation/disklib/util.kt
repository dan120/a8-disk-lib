/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.disklib

import com.google.common.hash.Hashing
import com.google.common.io.BaseEncoding
import com.google.common.io.Files
import com.google.common.primitives.Longs
import java.io.File
import java.util.zip.CRC32


/**
 * Creates an array indexed by interleave character with a value of sector number.
 */
fun createInterleaveMappingArray(interleave: String): IntArray {
    val map = IntArray(19)
    for ((i, c) in interleave.withIndex()) {
        map[i+1] = convertSectorLetterToNumber(c)
    }
    return map
}

/**
 * Converts a sector letter (1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I) to a number.
 */
fun convertSectorLetterToNumber(c: Char): Int {
    return when (c) {
        in '1'..'9' -> c.toInt() - 48
        in 'A'..'I' -> c.toInt() - 55
        else -> -1
    }
}

/**
 * Converts a sector number (1-18) to a letter.
 */
fun convertSectorNumberToLetter(num: Int): Char {
    return when (num) {
        in 1..9 -> (num+48).toChar()
        else -> (num+55).toChar()
    }
}

fun getTrackNumberForAbsoluteSectorNumber(num: Int, spt: Int): Int {
    return (num - 1) / spt
}

fun getTrackSectorNumberForAbsoluteSectorNumber(num: Int, spt: Int): Int {
    return (num - 1) % spt + 1
}

fun getAbsoluteSectorForTrackAndSectorString(s: String, sectorsPerTrack: Int): Int? {
    if (s[0] == 't' && s[3] == 's') {
        try {
            val tn = s.substring(1, 3).toInt()
            val sn = s.substring(4, 6).toInt()
            return getAbsoluteSectorForTrackAndSectorIndex(tn, sn, sectorsPerTrack)
        } catch (e: NumberFormatException) {}
    }
    return null
}

fun getAbsoluteSectorForTrackAndSectorIndex(track: Int, sector: Int, sectorsPerTrack: Int): Int {
    return (track * sectorsPerTrack + sector)
}

/**
 * Calculates the CRC of sector data taking weak bytes into consideration.
 */
fun calculateDataCrc(data: ByteArray?, weakOffset: Int?): CRC32 {
    val c = CRC32()
    if (data != null && data.isNotEmpty()) {
        if (weakOffset != null && weakOffset > data.size) {
            throw RuntimeException("Weak offset is greater than sector data size!")
        }
        // update the CRC with the disk data (ignoring weak sector data)
        c.update(data, 0, weakOffset ?: data.size)
    }
    return c
}

fun createCRC32BString(file: File): String {
    return createCRC32BString(Files.hash(file!!, Hashing.crc32()).padToLong())
}

fun createCRC32BString(crc32: CRC32): String {
    return createCRC32BString(crc32.value)
}

fun createCRC32BString(crc32: Long): String {
    val b = Longs.toByteArray(crc32)
    // reverse order of byte array for crc32b
    val crc32b = ByteArray(Long.SIZE_BYTES)
    for (i in 0 until Long.SIZE_BYTES) {
        crc32b[i] = b[Long.SIZE_BYTES - i - 1]
    }
    return BaseEncoding.base16().lowerCase().encode(b).substring(8)
}

fun createSectorSummary(disk: Disk): String {
    val sectorsPerTrack = disk.sectorsPerTrack
    val maxSectorCount = sectorsPerTrack * disk.trackCount
    val sb = StringBuilder()

    for (i in 1..maxSectorCount) {
        val sectorList = disk.getSector(i)

        if (sectorList.isEmpty()) {
            sb.append("B")
        } else if (sectorList.size > 1) {
            sb.append("P")
        } else {
            val sector = sectorList.first()
            if (sector.isEmpty()) {
                if (sector.hasError) {
                    sb.append("e")
                } else {
                    sb.append(".")
                }
            } else {
                if (sector.hasError) {
                    sb.append("E")
                } else {
                    sb.append("O")
                }
            }
        }
    }

    return sb.toString()
}

fun createProtectionSummary(disk: Disk): String {
    val sb = StringBuilder()

    // read the disk image
    val phaSectors: MutableSet<Int> = mutableSetOf()
    val crcSectors: MutableSet<Int> = mutableSetOf()
    val misSectors: MutableSet<Int> = mutableSetOf()
    val delSectors: MutableSet<Int> = mutableSetOf()
    val msfSectors: MutableSet<Int> = mutableSetOf()
    val wekSectors: MutableSet<Int> = mutableSetOf()
    val lonSectors: MutableSet<Int> = mutableSetOf()
    val sectorsPerTrack = disk.sectorsPerTrack

    // analyze
    disk.tracks.forEach {
        val missingSectors: MutableList<Int> = mutableListOf()
        for (i in 1..sectorsPerTrack) {
            missingSectors.add(i)
        }

        val trackIdx = it.number
        it.sectors.forEach {
            val secNum = trackIdx * sectorsPerTrack + it.number
            if (missingSectors.contains(it.number)) {
                missingSectors.remove(it.number)
                if (it.crcError) {
                    crcSectors.add(secNum)
                }
                if (it.isDeleted) {
                    delSectors.add(secNum)
                }
                if (it.isMissing) {
                    msfSectors.add(secNum)
                }
                if (it.hasWeakData) {
                    wekSectors.add(secNum)
                }
                if (it.isLong) {
                    lonSectors.add(secNum)
                }
            } else {
                phaSectors.add(secNum)
            }
        }
        if (missingSectors.size > 0) {
            misSectors.addAll(missingSectors.map {
                trackIdx * sectorsPerTrack + it
            })
        }
    }

    // generate protection info string
    if (crcSectors.size > 0) {
        sb.append("crc(").append(createStringRange(crcSectors.sorted())).append(")")
    }
    if (delSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("del(").append(createStringRange(delSectors.sorted())).append(")")
    }
    if (lonSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("lon(").append(createStringRange(lonSectors.sorted())).append(")")
    }
    if (misSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("mis(").append(createStringRange(misSectors.sorted())).append(")")
    }
    if (msfSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("msf(").append(createStringRange(msfSectors.sorted())).append(")")
    }
    if (phaSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("pha(").append(createStringRange(phaSectors.sorted())).append(")")
    }
    if (wekSectors.size > 0) {
        if (sb.isNotEmpty()) sb.append(",")
        sb.append("wek(").append(createStringRange(wekSectors.sorted())).append(")")
    }

    return if (sb.isNotEmpty()) sb.append("\n").toString() else "n\n"
}

fun createStringRange(numbers: List<Int>): String {
    var start: Int? = null
    var end: Int? = null
    val sb = StringBuilder()

    for (it in numbers) {
        //initialize
        if (start == null || end == null) {
            start = it
            end = it
        }
        //next number in range
        else if (end == it-1) {
            end = it
        }
        //there's a gap
        else {
            when (start) {
                //range length 1
                end -> sb.append(start).append(",")
                //range length 2
                end-1 -> sb.append(start).append(",").append(end).append(",")
                //range length 2+
                else -> sb.append(start).append("-").append(end).append(",")
            }
            start = it
            end = it
        }
    }

    if (start == end) {
        sb.append(start)
    } else if (end != null && start == end-1) {
        sb.append(start).append(",").append(end)
    } else {
        sb.append(start).append("-").append(end)
    }

    return sb.toString()
}

fun hexStringToByteArray(s: String): ByteArray {
    val len = s.length
    val data = ByteArray(len / 2)
    var i = 0
    while (i < len) {
        data[i / 2] = ((Character.digit(s[i], 16) shl 4)
                + Character.digit(s[i + 1], 16)).toByte()
        i += 2
    }
    return data
}
