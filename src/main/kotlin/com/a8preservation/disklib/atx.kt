/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.disklib

import java.io.*

data class ATXSectorHeader(val number: Int, val status: Int, val position: Int, val data: Int) {
    val hasExtendedData = ((status shr 6) and 1) > 0
    val isMissingData = ((status shr 4) and 1) > 0
}

/**
 * Reads an ATX file.
 *
 * Parses an Atari 8-bit ATX disk image and returns a representative Disk object.
 *
 * @param input the input stream to read the data from
 */
fun readATX(input: InputStream, name: String? = null): Disk {
    var fileIndex = 0

    val fileBuf: BufferedInputStream = input.buffered()

    if (fileBuf.read() != 'A'.toInt() || fileBuf.read() != 'T'.toInt() || fileBuf.read() != '8'.toInt() || fileBuf.read() != 'X'.toInt())
        throw IOException("Data not in ATX format")

    // read file header
    val version = fileBuf.read2ByteLittleEndian()
    if (version != 1)
        throw IOException("Unsupported ATX version")
    fileBuf.read2ByteLittleEndian() // minVersion
    fileBuf.read2ByteLittleEndian() // creator
    fileBuf.read2ByteLittleEndian() // creatorVersion
    fileBuf.read4ByteLittleEndian() // flags
    fileBuf.read2ByteLittleEndian() // imageType
    val density = when (fileBuf.read()) {
        0x01 -> DiskDensity.MEDIUM
        0x02 -> DiskDensity.DOUBLE
        else -> DiskDensity.SINGLE
    } // density
    fileBuf.skip(1)              // reserved
    fileBuf.read4ByteLittleEndian() // imageId
    fileBuf.read2ByteLittleEndian() // imageVersion
    fileBuf.skip(2)              // reserved
    val fileHeaderSize = fileBuf.read4ByteLittleEndian()
    fileBuf.skip((fileHeaderSize - 32).toLong())
    fileIndex += fileHeaderSize

    val tracks: MutableList<Track> = mutableListOf()

    // read all data records
    while (fileBuf.available() > 0) {
        // read full data record into buffer
        val size = fileBuf.read4ByteLittleEndian()
        val dataRecordBuf = ByteArray(size)
        fileBuf.read(dataRecordBuf, 4, size - 4)

        // read data record header
        val dataRecordType = dataRecordBuf.read2ByteLittleEndian(4)
        var dataRecordIdx = 8 // skip reserved

        // only read track data records
        if (dataRecordType == 0x00) {
            // read track header
            val trackNum = dataRecordBuf.readByte(dataRecordIdx++)
            dataRecordIdx++ // reserved
            val sectorCount = dataRecordBuf.read2ByteLittleEndian(dataRecordIdx)
            dataRecordIdx += 6
            val encoding = when (dataRecordBuf.read4ByteLittleEndian(dataRecordIdx)) {
                2 -> TrackEncoding.MFM
                else -> TrackEncoding.FM
            } // flags
            dataRecordIdx += 4
            val headerSize = dataRecordBuf.read4ByteLittleEndian(dataRecordIdx)
            dataRecordIdx += 12 // skip reserved

            val sectorLists = mutableListOf<ATXSectorHeader>()
            val weakOffsets = mutableMapOf<Int,Int>()

            // read all track chunks
            var terminatorFound = false
            while (!terminatorFound) {
                val chunkLen = dataRecordBuf.read4ByteLittleEndian(dataRecordIdx)
                val chunkBuf = dataRecordBuf.copyOfRange(dataRecordIdx, dataRecordIdx+chunkLen)
                dataRecordIdx += chunkLen

                if (chunkLen == 0) {
                    terminatorFound = true
                } else {
                    var chunkIdx = 4
                    val chunkType = chunkBuf.readByte(chunkIdx++)
                    val chunkSectorIndex = chunkBuf.readByte(chunkIdx++)
                    val chunkHeaderData = chunkBuf.read2ByteLittleEndian(chunkIdx)
                    chunkIdx += 2

                    when (chunkType) {
                        0x00 -> {
                            // sector data
                        }
                        0x01 -> {
                            for (i in 0 until sectorCount) {
                                sectorLists.add(ATXSectorHeader(
                                    chunkBuf.readByte(chunkIdx),
                                    chunkBuf.readByte(chunkIdx+1),
                                    chunkBuf.read2ByteLittleEndian(chunkIdx + 2),
                                    chunkBuf.read4ByteLittleEndian(chunkIdx + 4)
                                ))
                                chunkIdx += 8
                            }
                        }
                        0x10 -> {
                            weakOffsets.put(chunkSectorIndex, chunkHeaderData)
                        }
                        0x11 -> {
                            println("Extended sector header")
                            // extended sector header
                        }
                        else -> {
                            println("Unknown chunk type")
                            // unknown chunk type
                        }
                    }
                }
            }

            val sectorList = mutableListOf<Sector>()

            if (sectorLists.size > 0) {
                for ((i,h) in sectorLists.withIndex()) {
                    var data: ByteArray? = null
                    if (!h.isMissingData) {
                        data = dataRecordBuf.copyOfRange(h.data, h.data + 128)
                    }
                    val s = Sector(h.number, data, h.position, h.status)
                    if (weakOffsets.containsKey(i)) {
                        s.weakOffset = weakOffsets[i]
                    }
                    sectorList.add(s)
                }
            }
            tracks.add(Track(trackNum, encoding, sectorList))
        }
    }

    input.close()

    return Disk(density, tracks, name)
}

fun writeATX(file: File, disk: Disk) {
    val out = FileOutputStream(file);

    // write header
    out.write(byteArrayOf(0x41, 0x54, 0x38, 0x58)) // magic
    write2Bytes(out, 1) // version
    write2Bytes(out, 1) // min_version
    write2Bytes(out, 0x74) // creator
    write2Bytes(out, 1) // creator_version
    write4Bytes(out, 0) // flags
    write2Bytes(out, 0) // image_type
    writeByte(out, when (disk.density) {
        DiskDensity.SINGLE -> 0
        DiskDensity.MEDIUM -> 1
        DiskDensity.DOUBLE -> 2
    }) // density
    writeByte(out,0)        // reserved
    write4Bytes(out, 0)     // image_id
    write2Bytes(out, 0)     // image_version
    write2Bytes(out, 0)     // reserved
    write4Bytes(out, 48)    // start
    write4Bytes(out, calculateImageLength(disk)) // end
    out.write(byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)) // reserved

    // write track data
    for (track in disk.tracks) {
        // write data record header
        write4Bytes(out, calculateTrackRecordLength(track)) // track data record length
        write2Bytes(out, 0)            // type: track
        write2Bytes(out, 0)            // reserved
        // write track data header
        writeByte(out, track.number)        // track number
        writeByte(out,0)               // reserved
        write2Bytes(out, track.sectorCount) // sector count
        write2Bytes(out, 0)            // rate
        write2Bytes(out, 0)            // reserved
        write4Bytes(out, when (track.encoding) {
            TrackEncoding.MFM -> 2
            else -> 0
        })            // flags
        write4Bytes(out, 32)           // header size
        out.write(byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)) // reserved
        // write sector list chunk header
        write4Bytes(out, 8 + track.sectors.size * 8) // length
        writeByte(out, 1)                            // type: sector list
        writeByte(out, 0)                            // sector index
        write2Bytes(out, 0)                          // header data
        // write sector headers
        var i = 8 + 24 + 8 + 8 + (track.sectors.size * 8)
        for (sector in track.sectors) {
            writeByte(out, sector.number) // sector number
            writeByte(out, sector.fdcStatus) // sector status
            write2Bytes(out, sector.position) // sector position
            write4Bytes(out, i) // start data
            i += sector.data!!.size
        }
        // write sector data chunk header
        write4Bytes(out, 8 + 128 * track.sectors.size) // length
        writeByte(out, 0) // type: sector data
        writeByte(out, 0) // sector index
        write2Bytes(out, 0) // header data
        // write sector data
        val weakSectors = ArrayList<Sector>()
        for (sector in track.sectors) {
            out.write(sector.data)
            if (sector.hasWeakData) {
                weakSectors.add(sector)
            }
        }
        // write extended data chunks for sectors with weak data
        for (sector in weakSectors) {
            write4Bytes(out, 8)
            writeByte(out, 0x10)
            writeByte(out, track.sectors.indexOf(sector))
            write2Bytes(out, sector.weakOffset!!)
        }
        // write terminator chunk
        out.write(byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00))
    }

    out.close()
}

fun calculateImageLength(disk: Disk): Int {
    var len = 48
    for (track in disk.tracks) {
        len += calculateTrackRecordLength(track)
    }
    return len
}

fun calculateTrackRecordLength(track: Track): Int {
    return  8 +  // track record header
            24 + // track data header
            8 +  // sector list chunk header
            track.sectorCount * 8 + // sector headers
            8 + // sector chunk header
            track.sectorCount * 128 + // sector data
            track.sectors.count { it.hasWeakData } * 8 + // weak sector chunks
            8 // terminator
}
