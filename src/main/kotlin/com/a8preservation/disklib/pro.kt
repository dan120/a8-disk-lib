package com.a8preservation.disklib

import java.io.IOException
import java.io.InputStream
import java.lang.IllegalArgumentException

const val DEFAULT_SD_INTERLEAVE = "13579BDFH2468ACEGI"
const val DEFAULT_ED_INTERLEAVE = "13579BDFHJLNP2468ACEGIKMOQ"

fun readPRO(input: InputStream, forcedInterleave: String? = null, name: String? = null): Disk {
    val buffer = input.buffered()

    // read bits of the file header we care about
    val totalSectorCount =  buffer.read2ByteBigEndian();
    if (buffer.read() != 'P'.toInt())
        throw IOException("Data not in PRO format")
    val version = buffer.read()
    val phantomMode = buffer.read()
    val delay = buffer.read()
    var originalSectorCount = buffer.read2ByteBigEndian()
    if (originalSectorCount == 0) {
        originalSectorCount = if (totalSectorCount >= 1040) 1040 else 720
    }
    for (i in 0..7) {
        buffer.read()
    }

    var density = DiskDensity.SINGLE
    var spt = 18
    var interleave = forcedInterleave ?: DEFAULT_SD_INTERLEAVE
    var trackEncoding = TrackEncoding.FM
    if (originalSectorCount == 1040) {
        density = DiskDensity.MEDIUM
        spt = 26
        interleave = forcedInterleave ?: DEFAULT_ED_INTERLEAVE
        trackEncoding = TrackEncoding.MFM
    }

    // temp structures to hold disk data
    val trackMap: MutableMap<Int,MutableList<Sector>> = mutableMapOf() // track number -> list of sectors
    val phantomIndexMap: MutableMap<Int,Int> = mutableMapOf() // phantom index -> absolute sector number
    val phantomMap: MutableMap<Int,PhantomSector> = mutableMapOf() // phantom index -> sector data
    val data = ByteArray(128)

    // build track map by iterating through all sectors
    for (sectorNum in 1..totalSectorCount) {
        if (sectorNum <= originalSectorCount) {
            val trackNum = getTrackNumberForAbsoluteSectorNumber(sectorNum, spt)
            var sectorList = trackMap.get(trackNum)

            // create sector list if not found
            if (sectorList == null) {
                sectorList = mutableListOf()
                trackMap[trackNum] = sectorList
            }

            // read sector header & data
            val snum = getTrackSectorNumberForAbsoluteSectorNumber(sectorNum, spt)
            val sectorHeader = ProSectorHeader(buffer.read(), buffer.read(), buffer.read2ByteBigEndian(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read())
            buffer.read(data, 0, 128)

            if (sectorHeader.phantom1 > 0) {
                phantomIndexMap[sectorHeader.phantom1] = sectorNum
                if (sectorHeader.phantom2 > 0) {
                    phantomIndexMap[sectorHeader.phantom2] = sectorNum
                    if (sectorHeader.phantom3 > 0) {
                        phantomIndexMap[sectorHeader.phantom3] = sectorNum
                        if (sectorHeader.phantom4 > 0) {
                            phantomIndexMap[sectorHeader.phantom4] = sectorNum
                            if (sectorHeader.phantom5 > 0) {
                                phantomIndexMap[sectorHeader.phantom5] = sectorNum
                            }
                        }
                    }
                }
            }

            // create sector object if sector is flagged as found
            if (((sectorHeader.hwStatus and 16) shr 4) > 0) {
                sectorList.add(Sector(snum, data.copyOf(), calculateSectorPosition(snum, spt, interleave), sectorHeader.hwStatus.inv() and 60))
            }
        } else {
            val phantomIndex = sectorNum - originalSectorCount
            val sectorHeader = ProSectorHeader(buffer.read(), buffer.read(), buffer.read2ByteBigEndian(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read(), buffer.read())
            buffer.read(data, 0, 128)
            phantomMap.put(phantomIndex, PhantomSector(sectorHeader, data.copyOf()))
        }
    }

    input.close()

    // process phantom sectors
    for (phantomIndex in phantomMap.keys) {
        val sectorNum = phantomIndexMap[phantomIndex]
        if (sectorNum != null) {
            insertPhantomSectorIntoSectorList(sectorNum, phantomMap[phantomIndex], trackMap[getTrackNumberForAbsoluteSectorNumber(sectorNum, spt)], spt, interleave)
        }
    }

    // build tracks
    val trackList = mutableListOf<Track>()
    for (trackNum in trackMap.keys) {
        val sl = trackMap[trackNum]
        if (sl != null) {
            trackList.add(Track(trackNum, trackEncoding, sl.sortedBy { it.position }))
        }
    }

    return Disk(density, trackList, name)
}

fun calculateSectorPosition(trackSectorNumber: Int, spt: Int, interleave: String): Int {
    if (interleave.length < spt) {
        throw IllegalArgumentException("Interleave sector count cannot be less than sectors per track")
    }
    val interleavedIndex = (interleave.indexOf(convertSectorNumberToLetter(trackSectorNumber)) + 1)
    return (interleavedIndex - 1) * (26042 / spt)
}

fun insertPhantomSectorIntoSectorList(number: Int, sector: PhantomSector?, sectorList: MutableList<Sector>?, spt: Int, interleave: String) {
    if (sectorList != null && sector != null) {
        val trackSectorNum = getTrackSectorNumberForAbsoluteSectorNumber(number, spt)
        val firstMissingNum = findFirstMissingSectorNumber(sectorList, spt)
        if (firstMissingNum > -1) {
            sectorList.add(Sector(trackSectorNum, sector.data, calculateSectorPosition(firstMissingNum, spt, interleave)))
        } else {
            val currentSector = sectorList.find { it.number == trackSectorNum }
            if (currentSector != null) {
                var pos = currentSector.position + 13021
                if (pos > 26042) {
                    pos -= 26042
                }
                sectorList.add(Sector(trackSectorNum, sector.data, pos))
            } else {
                throw RuntimeException("Unable to identify position to insert phantom/duplicate sector $number")
            }
        }
    }
}

fun findFirstMissingSectorNumber(sectorList: MutableList<Sector>?, spt: Int): Int {
    if (sectorList != null) {
        if (sectorList.size == 0) {
            return 1
        }
        for (i in 1..spt) {
            sectorList.find { it.number == i } ?: return i
        }
    }
    return -1
}

data class ProSectorHeader(val cmdStatus: Int = 0, val hwStatus: Int = 0, val timeout: Int = 0, val unknown1: Int = 0, val phantomCount: Int = 0, val phantom4: Int = 0, val phantom1: Int = 0, val phantom2: Int = 0, val phantom3: Int = 0, val unknown2: Int = 0, val phantom5: Int = 0)
data class PhantomSector(val header: ProSectorHeader, val data: ByteArray?)
