package com.a8preservation.disklib

import java.io.BufferedInputStream
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

/**
 * Reads an SCP (SuperCard Pro) file.
 *
 * Parses an SCP disk image and returns a representative Disk object.
 *
 * @param input the input stream to read the data from
 */
fun readSCP(input: InputStream): Disk {
    val fileBuf: BufferedInputStream = input.buffered()

    // read magic
    if (fileBuf.read() != 'S'.toInt() || fileBuf.read() != 'C'.toInt() || fileBuf.read() != 'P'.toInt())
        throw IOException("Data not in SCP format")

    var fileIdx = 3

    // read disk definition header
    val revision = fileBuf.read()
    val diskType = fileBuf.read()
    val numRevs = fileBuf.read()
    val startTrack = fileBuf.read()
    val endTrack = fileBuf.read()
    val trackCount = endTrack - startTrack
    val flags = fileBuf.read()
    val driveRpm = if ((flags and 4) shr 2 == 1) 360 else 300
    val bitCellWidth = fileBuf.read()
    val numHeads = fileBuf.read()
    val captureResolution = fileBuf.read()
    val checksum = fileBuf.read4ByteLittleEndian()

    fileIdx += 13

    if (bitCellWidth != 0) {
        throw IOException("Bit cell widths other than 16-bit not supported!")
    }

    // build map of track data offsets
    val trackMap = mutableListOf<Int>()
    for (i in 0..trackCount) {
        val offset = fileBuf.read4ByteLittleEndian()
        trackMap.add(offset)
        fileIdx += 4
    }

    val tracks = mutableListOf<Track>()

    for (offset in trackMap) {
        if (offset > 0) {
            // skip to beginning of track header
            for (i in 0 until offset - fileIdx) {
                fileBuf.read()
                fileIdx++
            }

            val trackStartIdx = fileIdx

            // read track magic
            if (fileBuf.read() != 'T'.toInt() || fileBuf.read() != 'R'.toInt() || fileBuf.read() != 'K'.toInt())
                throw IOException("Found invalid track record")

            // read track number
            val trackNum = fileBuf.read()
            println("Track $trackNum")
            fileIdx += 4

            // read revolution information
            val trackEntries = mutableListOf<TrackDataEntry>()
            val fluxDatas = mutableListOf<IntArray>()

            for (i in 0 until numRevs) {
                trackEntries.add(TrackDataEntry(fileBuf.read4ByteLittleEndian(), fileBuf.read4ByteLittleEndian(), fileBuf.read4ByteLittleEndian()))
                fileIdx += 12
            }

            // read flux data
            for (te in trackEntries) {
                val trackLenInBytes = te.trackLen * 2 // we can assume this because width is 16-bites
                val fluxData = IntArray(te.trackLen)

                println("Rev duration: ${te.revDuration}, Track len: ${te.trackLen}")

                // skip any padding between track entries and flux data
                while (fileIdx < trackStartIdx + te.dataOffset) {
                    fileBuf.read()
                    fileIdx++
                }

                for (i in 0 until te.trackLen) {
                    fluxData[i] = fileBuf.read2ByteBigEndian()
                }
                fluxDatas.add(fluxData)
                fileIdx += trackLenInBytes
            }

            tracks.add(createAggregateTrackFromSCPFluxDatas(trackNum, fluxDatas, driveRpm))
        }
    }

    return Disk(DiskDensity.SINGLE, tracks)
}

fun createAggregateTrackFromSCPFluxDatas(trackNum: Int, fluxDatas: List<IntArray>, driveRpm: Int): Track {
    // TODO: analyze all flux streams for track and aggregate
    val fd = mutableListOf<FluxData>()
    for (fluxData in fluxDatas) {
        val cfd = convertSCPFluxTransitionsToNanoseconds(fluxData, driveRpm)
        fd.add(FluxData(cfd, convertFluxStreamToBytes(cfd)))
        if (trackNum == 0) {
            println("Track $trackNum")
            println(convertFluxStreamToBitString(cfd))
        }
    }
    return Track(trackNum, TrackEncoding.FM, mutableListOf(), driveRpm, fd)
}

fun convertSCPFluxTransitionsToNanoseconds(data: IntArray, driveRpm: Int): DoubleArray {
    val conversion = driveRpm / 288.0
    return data.map { it * 25 * conversion / 1000.0 }.toDoubleArray()
}

data class TrackDataEntry(val revDuration: Int, val trackLen: Int, val dataOffset: Int)

fun main() {
    val disk = readSCP(FileInputStream("C:\\Users\\dan\\Desktop\\Action Quest (1982)(JV Software)(US)_A8.scp"))
    println(disk.trackCount)
}
