/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.disklib

import java.io.*

/**
 * Reads an ATR file.
 *
 * Parses an Atari 8-bit ATR disk image and returns a representative Disk object.
 *
 * @param input the input stream to read the data from
 */
fun readATR(input: InputStream, name: String? = null): Disk {
    val buffer = input.buffered()

    // read bits of the header we care about
    if (buffer.read2ByteLittleEndian() != 0x0296) throw IOException("Data not in ATR format")
    val imageSize = buffer.read2ByteLittleEndian() * 0x10
    val sectorSize = buffer.read2ByteLittleEndian()

    // read remaining header
    buffer.skip(10)

    // set disklib params
    var density = DiskDensity.SINGLE
    var sectorsPerTrack = 18
    var trackEncoding = TrackEncoding.FM
    when (imageSize) {
        92160 -> {
        }
        133120 -> {
            density = DiskDensity.MEDIUM
            sectorsPerTrack = 26
            trackEncoding = TrackEncoding.MFM
        }
        183936 -> {
            density = DiskDensity.DOUBLE
            trackEncoding = TrackEncoding.MFM
        }
        else -> throw IOException("Invalid or unsupported ATR file of size $imageSize")
    }

    // read sector data
    val d =  Disk(density, (0 until imageSize / sectorSize / sectorsPerTrack).map {
            Track(it, trackEncoding, (1 until sectorsPerTrack+1).map {
                val ba = ByteArray(sectorSize)
                buffer.read(ba)
                Sector(it, ba)
            })
        },
        name
    )

    input.close()

    return d
}

fun writeATR(file: File, disk: Disk) {
    val out = FileOutputStream(file);

    // write header
    write2Bytes(out, 0x0296) // magic
    write2Bytes(out, when (disk.density) {
        DiskDensity.MEDIUM -> 0x2080
        DiskDensity.DOUBLE -> 0x2ce8
        else -> 0x1680
    })                            // size of image (in paragraphs - size/$10)
    val sectorSize = when (disk.density) {
        DiskDensity.DOUBLE -> 0x100
        else -> 0x80
    }
    write2Bytes(out, sectorSize) // sector size
    writeByte(out, 0x00)    // high part of size
    write4Bytes(out, 0x00)  // CRC
    write4Bytes(out, 0x00)  // unused
    writeByte(out, 0x00)    // write-only

    // write data
    for (t in 0 until disk.trackCount) {
        val track = disk.getTrack(t)
        for (s in 1..disk.sectorsPerTrack) {
            val sector = track.getFirstSector(s)
            if (sector != null && sector.hasData) {
                out.write(sector.data)
            } else {
                for (i in 0 until sectorSize) {
                    out.write(0)
                }
            }
        }
    }

    out.close()
}